# Simple Pki
Création d'une PKI simple
Ce travail est basé sur ce projet tout en y apportant quelques modifications :
https://pki-tutorial.readthedocs.io/en/latest/


Pour commencer il suffit de cloner le projet.
```
git clone https://gitlab.com/klsx/simple_pki.git
cd simple_pki
```

# Root CA :
Permet de créer l'authorité de certification racine

### Création des dossiers
```
mkdir -p ca/root-ca/private ca/root-ca/db crl certs
chmod 700 ca/root-ca/private
```
Contenu des dossiers :

`ca` -> Ressources des authorité de certifications

`crl` -> Certificats de revocation

`certs` -> Certificats utilisateurs


### Création base de données
```
cp /dev/null ca/root-ca/db/root-ca.db
cp /dev/null ca/root-ca/db/root-ca.db.attr
echo 01 > ca/root-ca/db/root-ca.crt.srl
echo 01 > ca/root-ca/db/root-ca.crl.srl
```
La base de données doit être créer pour que la commande `openssl ca` fonctionne

### Création requête CA
```
openssl req -new \
    -config etc/root-ca.conf \
    -out ca/root-ca.csr \
    -keyout ca/root-ca/private/root-ca.key
```
`openssl req` permet de créer une requête d'un certificat (.csr) mais en aucun cas c'est un certificat (.crt) signé.

### Création certificat CA
```
openssl ca -selfsign \
    -config etc/root-ca.conf \
    -in ca/root-ca.csr \
    -out ca/root-ca.crt \
    -extensions root_ca_ext
```
`openssl ca` permet de créer une certificat (.crt) depuis une requête.
`-selfsign` permet d'autosigner le certificat.


Nous avons fini de créer l'authorité de certification racine CA Root.
Il nous faut ensuite une entité intermidiaire pour signer nos certificats utilisateurs.
# Création CA intermédiraire

### Création dossiers
```
mkdir -p ca/signing-ca/private ca/signing-ca/db crl certs
chmod 700 ca/signing-ca/private
```

### Création base de données
```
cp /dev/null ca/signing-ca/db/signing-ca.db
cp /dev/null ca/signing-ca/db/signing-ca.db.attr
echo 01 > ca/signing-ca/db/signing-ca.crt.srl
echo 01 > ca/signing-ca/db/signing-ca.crl.srl
```

### Création requete CA
```
openssl req -new \
    -config etc/signing-ca.conf \
    -out ca/signing-ca.csr \
    -keyout ca/signing-ca/private/signing-ca.key
```
### Création certificat CA
```
openssl ca \
    -config etc/root-ca.conf \
    -in ca/signing-ca.csr \
    -out ca/signing-ca.crt \
    -extensions signing_ca_ext
```

# Génération d'un certificat TLS

### Création requete TLS serveur
```
SAN=DNS:www.simple.org \
openssl req -new \
    -config etc/server.conf \
    -out certs/simple.org.csr \
    -keyout certs/simple.org.key
```
Informations à rentrer : DC=org,DC=simple,O=Simple Inc, CN=www.simple.org le reste est vide.
Le CommonName (CN) est très important, sinon le certificat ne fonctionnera pas correctement.

Cette commande va nous générer la clé privé dans le dossier certs. 

### Création certificat TLS serveur
```
openssl ca \
    -config etc/signing-ca.conf \
    -in certs/simple.org.csr \
    -out certs/simple.org.crt \
    -extensions server_ext
```

Le certificat sera généré dans le dossier certs avec l'extension .crt 


### Création d'un bundle (serveur.crt signing.crt)
```
cat certs/simple.org.crt ca/signing-ca.crt > certs/bundle.crt
```
Ce bundle nous permet d'avoir une `fullchain` des certificats comme nous passons par une CA intermédiaire


# Config Apache virtualhost :

Ouvrir virtualhost default-ssl

`sudo nano /etc/apache2/site-available/default-ssl`

```
NameVirtualHost 0.0.0.0:80
NameVirtualHost 0.0.0.0:443

<VirtualHost 0.0.0.0:80>
DocumentRoot /var/www/html/
ServerName test
</VirtualHost>

<VirtualHost 0.0.0.0:443>

DocumentRoot /var/www/html/
ServerName www.simple.org
SSLEngine on
SSLProtocol All -SSLv2 -SSLv3
SSLCertificateFile /XX/XX/simple_pki/certs/bundle.crt
SSLCertificateKeyFile /XX/XX/simple_pki/certs/simple.org.key

</VirtualHost>

```
Activer la nouvelle configuration

`a2ensite default-ssl`
Puis recharger apache2
`sudo systemctl reload apache2`

# Mot clés
CA -> Authorité de certification peut être, racine ou intermédiraire en fonctionne de la personne qu'il l'a signé.
Si auto-signé c'est une CA root sinon elle est intermédiraire.

CA Root -> Authorité de certification racine.

crt -> Certificat signé.

csr -> Requête d'un certificat non signé.

crl -> Certificat de révocation.
key -> Clé privé à ne surotut pas partager.