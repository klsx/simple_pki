#!/bin/bash
function root_ca {

	# Création des dossiers
	mkdir -p ca/root-ca/private ca/root-ca/db crl certs
	chmod 700 ca/root-ca/private

	# Création de la base de données
	cp /dev/null ca/root-ca/db/root-ca.db
	cp /dev/null ca/root-ca/db/root-ca.db.attr
	echo 01 > ca/root-ca/db/root-ca.crt.srl
	echo 01 > ca/root-ca/db/root-ca.crl.srl


	# Création requete CA
	openssl req -new \
		-config etc/root-ca.conf \
		-passout pass:$PASS \
		-out ca/root-ca.csr \
		-keyout ca/root-ca/private/root-ca.key

	# Création certificat CA
	openssl ca -selfsign \
		-config etc/root-ca.conf \
		-in ca/root-ca.csr \
		-passin pass:$PASS \
		-out ca/root-ca.crt \
		-extensions root_ca_ext

}

function intermediate_ca {
	# Création des dossiers
	mkdir -p ca/signing-ca/private ca/signing-ca/db crl certs
	chmod 700 ca/signing-ca/private

	# Création de la base de données
	cp /dev/null ca/signing-ca/db/signing-ca.db
	cp /dev/null ca/signing-ca/db/signing-ca.db.attr
	echo 01 > ca/signing-ca/db/signing-ca.crt.srl
	echo 01 > ca/signing-ca/db/signing-ca.crl.srl

	# Création requete CA
	openssl req -new \
		-config etc/signing-ca.conf \
		-passout pass:$PASS \
		-out ca/signing-ca.csr \
		-keyout ca/signing-ca/private/signing-ca.key

	# Création certificat CA
	openssl ca \
		-config etc/root-ca.conf \
		-in ca/signing-ca.csr \
		-passin pass:$PASS \
		-out ca/signing-ca.crt \
		-extensions signing_ca_ext
}

function generate_tls_certificat {

	# Création requete TLS serveur
	SAN=DNS:$CN \
	openssl req -new \
		-config etc/server.conf \
		-passout pass:$PASS \
		-out certs/$CN.csr \
		-subj "/DC=org/DC=simple/O=Simple Inc/CN=$CN" \
		-keyout certs/$CN.key

	# Création certificat TLS serveur
	openssl ca \
		-config etc/signing-ca.conf \
		-in certs/$CN.csr \
		-passin pass:$PASS \
		-out certs/$CN.crt \
		-extensions server_ext

	# Création d'un bundle (serveur.crt signing.crt)
	cat certs/$CN.crt ca/signing-ca.crt > certs/${CN}_bundle.crt

	printf "\n\n=== TLS certs infos for $CN ===\n"
	printf "Cert path : $(readlink -f certs/$CN.crt)\n"
	printf "Bundle path : $(readlink -f certs/$CN_bundle.crt)\n"
	printf "Cert private key path : $(readlink -f certs/$CN.key)\n"
	printf "\n=== CA root certificate ===\n"
	printf "$(readlink -f ca/root-ca.crt)\n"
}

function intro {

	printf "1) Generate a pki with a cert\n"
	# printf "2) Generate only a cert\n"
	printf "3) Quit\n"
	read -p "Choix > " input

	if [[ "${input}" -eq 1 ]] ; then
		read -p "Enter a passphrase > " passphrase
		PASS=$passphrase
		read -p "Enter a CN > " commonname
		CN=$commonname

		root_ca
		intermediate_ca
		generate_tls_certificat

	# elif [[ "${input}" -eq 2 ]] ; then
	#     read -p "Enter a passphrase > " passphrase
	#     PASS=$passphrase
	#     read -p "Enter a CN > " commonname
	#     CN=$commonname
	#     generate_tls_certificat

	# else
	#     echo "bye :)"
	fi
}

intro
